# from nn_model import NNModel
from roi_queue import ROIQueue

network = """[{
		"type": "conv",
		"filter_size": 5,
		"filter_count": 50,
		"stride": 1,
		"maintain_spatial": false
	},{
		"type": "conv",
		"filter_size": 5,
		"filter_count": 50,
		"stride": 1,
		"maintain_spatial": false
	},{

		"type": "fc",
		"count": 1024
	}]
"""

rq = ROIQueue('new_set.p')

print(rq.data_templates['train'])

exit()
with rq as gen:
    next(gen)
    try:
        while True:
            pass
    except KeyboardInterrupt:
        pass

exit()
NNModel(network, {'image':  [15, 15, 23, 1]}, 6).train(
    'new_set.p',
    learning_rate=1e-6,
    iterations=1e8,
    batch_size=32,
    angles = {
        'train': list(range(0, 360, 5)),
        'validate': list(range(0, 360, 90)),
        'test': list(range(0, 360, 180))
    }
)