import SimpleITK as sitk
import numpy as np
import math

class ImageHelper:

    @staticmethod
    def add_dim(self, ndarray):
        s = list(ndarray.shape)
        if len(s) > 3:
            return ndarray
        return np.expand_dims(ndarray, 0)

    @staticmethod
    def remove_dim(self, ndarray):
        s = list(ndarray.shape)
        if len(s) <= 3:
            return ndarray
        return np.squeeze(ndarray, 3)

    @staticmethod
    def crop_image(ndarray, target_shape):
        lookup = []
        current_shape = np.array(ndarray.shape)

        for idx, v in enumerate(target_shape):
            target = target_shape[idx]
            current = current_shape[idx]
            diff = current - target
            if diff < 0:
                raise ValueError("Cropping failed, target dimensions is larger than image dimensions")

            diff_pair = [int(diff / 2), int(diff / 2)]

            # if diff is odd, add one to the "right" side
            if diff % 2 == 1:
                diff_pair[1] += 1

            lookup.append(diff_pair)

        new_array = ndarray[
            lookup[0][0]:current_shape[0] - lookup[0][1],
            lookup[1][0]:current_shape[1] - lookup[1][1],
            lookup[2][0]:current_shape[2] - lookup[2][1],
        ]

        return new_array

    @staticmethod
    def pad_image(ndarray, target_shape, mode='repeat'):

        if mode is None:
            return ImageHelper.crop_image(ndarray, target_shape)

        lookup = []
        current_shape = np.array(ndarray.shape)

        for idx, v in enumerate(target_shape):
            target = target_shape[idx]
            current = current_shape[idx]
            diff = target - current
            if diff < 0:
                raise ValueError("Padding failed, target dimensions is smaller than image dimensions")

            diff_pair = [int(diff / 2), int(diff / 2)]

            # if diff is odd, add one to the "right" side
            if diff % 2 == 1:
                diff_pair[1] += 1

            lookup.append(diff_pair)

        if mode == 'zero':
            return np.lib.pad(ndarray, lookup, 'constant', constant_values=0)
        elif mode == 'repeat':
            return np.lib.pad(ndarray, lookup, 'symmetric')
        else:
            raise ValueError("Unrecognized padding mode {0}".format(mode))

    @staticmethod
    def scale_rois(image_path, roi_list, target_spacing, interpolation_strategy=sitk.sitkNearestNeighbor):

        print(image_path)

        im_original = ImageHelper.read_image(image_path, correct_spacing=False)

        sx, sy, sz = im_original.GetSpacing()
        scale_factor = sz / target_spacing

        x, y, z = im_original.GetSize()
        z = math.ceil(z * scale_factor)

        if scale_factor == 1:
            return im_original, roi_list

        f = sitk.ResampleImageFilter()
        f.SetReferenceImage(im_original)
        f.SetSize([x, y, z])
        f.SetOutputSpacing([sx, sy, target_spacing])
        f.SetOutputOrigin(im_original.GetOrigin())
        f.SetOutputDirection(im_original.GetDirection())
        f.SetInterpolator(interpolation_strategy)
        f.SetOutputPixelType(sitk.sitkFloat32)
        f.SetDefaultPixelValue(1024)
        f.SetTransform(sitk.Transform())
        im_new = f.Execute(im_original)

        # sitk.WriteImage(im_new, "test.nrrd")
        # im_new = sitk.Resample(im_original, transform, interpolation_strategy, 1024, sitk.sitkFloat32)

        for roi in roi_list:
            org_physical = im_original.TransformContinuousIndexToPhysicalPoint(roi.roi_center)
            new_idx = im_new.TransformPhysicalPointToIndex(org_physical)
            roi.roi_center = np.array(new_idx)
            roi.roi_size[2] *= scale_factor

        return im_new, roi_list


    @staticmethod
    def read_image(path, correct_spacing=False):
        im = sitk.ReadImage(path)
        ndarray = sitk.GetArrayFromImage(im)

        spacing = list(im.GetSpacing())
        direction = im.GetDirection()
        origin = im.GetOrigin()

        spacing_factor = 1
        if correct_spacing:
            if spacing[2] == 0.5 or spacing[2] == 0.625:
                ndarray = ndarray[::-2, :, :]
                spacing[2] *= 2
                spacing_factor = 1/2
        else:
            ndarray = ndarray[::-1, :, :]

        new_im = sitk.GetImageFromArray(ndarray)
        new_im.SetSpacing(spacing)
        new_im.SetDirection(direction)
        new_im.SetOrigin(origin)

        if correct_spacing:
            return spacing_factor, new_im
        return new_im