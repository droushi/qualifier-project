import numpy as np
import pandas as pd
import SimpleITK as sitk
import matplotlib.cm as cm
import matplotlib.pyplot as plt

from IPython import display

def res_class_dist(data, randseed):
    dataframes = []
    indices = list(data.keys())
    columns = ['0.5', '0.625', '1.0', '1.25']
    dataframe_labels = ['train', 'test']
    input_data = {}

    for dfl in dataframe_labels:
        input_data[dfl] = []
        for class_id in data:
            class_data = []
            for res_id in columns:
                class_data.append(data[class_id][dfl][res_id])
            input_data[dfl].append(class_data)

    for dfl in dataframe_labels:
        x = np.float32(np.array(input_data[dfl]))
        x /= np.expand_dims(np.sum(x, axis=1), 1)
        dataframes.append(
            pd.DataFrame(x,
                         index=indices,
                         columns=columns)
        )

    plot_clustered_stacked(dataframes, dataframe_labels,
                           title="Class Distribution against resolution ({0})".format(randseed))


# Statistical Variables
def get_mean(stat, skip=0, steps=3):

    if len(stat) < steps:
        return None

    x = list(reversed(stat))
    d = np.array(x[skip:skip + steps])
    val = d[:, 1]
    return sum(val) / steps


def get_fft_image(img):
    if type(img) is np.ndarray:
        img = sitk.GetImageFromArray(img)
    return sitk.ComplexToReal(sitk.FFTShift(sitk.ForwardFFT(sitk.FFTPad(img))))


def get_mean_diff(stat, steps=3):
    if len(stat) < steps * 2:
        return None

    x_new = get_mean(stat, skip=0, steps=steps)
    x_old = get_mean(stat, skip=steps, steps=steps)
    return x_new - x_old


def update_graph(loss_data, train_acc, test_acc, test_loss):
    display.clear_output(wait=True)

    fig, ax_arr = plt.subplots(2, sharex=True)


    d1 = np.array(loss_data)
    d2 = np.array(test_loss)
    x1_data = d1[:, 0]
    y1_data = d1[:, 1]
    x2_data = d2[:, 0]
    y2_data = d2[:, 1]
    ax_arr[0].set_ylabel('Loss')
    ax_arr[0].set_xlabel('Iterations')
    ax_arr[0].plot(x1_data, y1_data, 'r-', label="train loss")
    ax_arr[0].plot(x2_data, y2_data, 'g-', label="test loss")
    ax_arr[0].legend(framealpha=0.5, loc="lower left")


    d1 = np.array(train_acc)
    d2 = np.array(test_acc)
    x1_data = d1[:, 0]
    y1_data = d1[:, 1]
    x2_data = d2[:, 0]
    y2_data = d2[:, 1]
    ax_arr[1].set_ylabel('Accuracy')
    ax_arr[1].set_xlabel('Iterations')
    ax_arr[1].plot(x1_data, y1_data, 'r-', label="training")
    ax_arr[1].plot(x2_data, y2_data, 'g-', label="testing")
    ax_arr[1].legend(framealpha=0.5, loc="lower left")


    fig.set_size_inches(18.5, 10.5)

    plt.show()




def plot_clustered_stacked(dfall, labels=None, title="multiple stacked bar plot",  H="/", **kwargs):
    """Given a list of dataframes, with identical columns and index, create a clustered stacked bar plot.
labels is a list of the names of the dataframe, used for the legend
title is a string for the title of the plot
H is the hatch used for identification of the different dataframe"""

    n_df = len(dfall)
    n_col = len(dfall[0].columns)
    n_ind = len(dfall[0].index)
    axe = plt.subplot(111)

    for df in dfall : # for each data frame
        axe = df.plot(kind="bar",
                      linewidth=0,
                      stacked=True,
                      ax=axe,
                      legend=False,
                      grid=False,
                      **kwargs)  # make bar plots

    h,l = axe.get_legend_handles_labels() # get the handles we want to modify
    for i in range(0, n_df * n_col, n_col): # len(h) = n_col * n_df
        for j, pa in enumerate(h[i:i+n_col]):
            for rect in pa.patches: # for each index
                rect.set_x(rect.get_x() + 1 / float(n_df + 1) * i / float(n_col))
                rect.set_hatch(H * int(i / n_col)) #edited part
                rect.set_width(1 / float(n_df + 1))

    axe.set_xticks((np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2.)
    axe.set_xticklabels(df.index, rotation = 0)
    axe.set_title(title)

    # Add invisible data to add another legend
    n=[]
    for i in range(n_df):
        n.append(axe.bar(0, 0, color="gray", hatch=H * i))

    l1 = axe.legend(h[:n_col], l[:n_col], loc=[1.01, 0.5])
    if labels is not None:
        l2 = plt.legend(n, labels, loc=[1.01, 0.1])
    axe.add_artist(l1)
    plt.tight_layout()
    return axe