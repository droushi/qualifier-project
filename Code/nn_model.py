import json
import time
import pickle
import os
import random
import string

import numpy as np
import tensorflow as tf
from roi_queue import ROIQueue
from net_calc import NetCalc
from stat_helper import *

import pandas as pd
from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.metrics import classification_report


class NNModel:

    @staticmethod
    def gen_id(size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))


    def print_confusion_matrix(plabels, tlabels):
        """
            functions print the confusion matrix for the different classes
            to find the error...

            Input:
            -----------
            plabels: predicted labels for the classes...
            tlabels: true labels for the classes

            code from: http://stackoverflow.com/questions/2148543/how-to-write-a-confusion-matrix-in-python
        """
        plabels = pd.Series(plabels)
        tlabels = pd.Series(tlabels)

        # draw a cross tabulation...
        df_confusion = pd.crosstab(tlabels, plabels, rownames=['Actual'], colnames=['Predicted'], margins=True)

        # print df_confusion
        return df_confusion

    @staticmethod
    def confusionMatrix(text, Labels, y_pred, not_partial):
        y_actu = np.where(Labels[:] == 1)[1]
        df = NNModel.print_confusion_matrix(y_pred, y_actu)
        print("\n", df)
        # print plt.imshow(df.as_matrix())
        if not_partial:
            print("\n", classification_report(y_actu, y_pred))
        print("\n\t------------------------------------------------------\n")

    def evaluate_model(self, message, x, y):

        feed_dict = {
            self.Y: y
        }
        for idx in x:
            feed_dict[self.X[idx]] = x[idx]

        predictions = self.session.run([self.correct_pred], feed_dict=feed_dict)
        prediction = tf.argmax(self.model, 1)
        labels = prediction.eval(feed_dict, session=self.session)
        print(message, self.accuracy.eval(feed_dict, session=self.session), "\n")
        NNModel.confusionMatrix("Partial Confusion matrix", y, predictions[0], False)  # Partial confusion Matrix
        NNModel.confusionMatrix("Complete Confusion matrix", y, labels, True)  # complete confusion Matrix

    def build_from_saved_state(self, model_state_path):
        model_metadata_path = os.path.join(model_state_path, 'model.p')
        model_meta_data = pickle.load(open(model_metadata_path, 'rb'))
        # print('input_shape' not in model_meta_data)
        self.network_arch = model_meta_data['arch']
        self.model_state_path = os.path.join(model_state_path, 'model.ckpt')
        if 'input_shape' not in model_meta_data:
            self.input_shape = {'image': [15, 15, 23, 1]}
        if 'num_classes' not in model_meta_data:
            self.num_classes = 6

    def __init__(self, network_arch, input_shape, num_classes, model_state_path=None):

        if network_arch is None and model_state_path is not None:
            self.build_from_saved_state(model_state_path)
        else:
            self.model_state_path = None
            self.network_arch = json.loads(network_arch)
            self.num_classes = num_classes
            self.input_shape = input_shape

        self.graph = None
        self.graph_scope = NNModel.gen_id()

        self.model = None
        self.X = None
        self.Y = None
        self.correct_pred = None
        self.accuracy = None

        self.train_params = None
        self.model_meta_information = None

        self.network_builder = NetCalc(self.input_shape, self.num_classes, 32, False)

        self.graph = tf.Graph()
        with self.graph.as_default() as g:
            self.session = tf.Session(graph=self.graph)
            self.model = self.network_builder.build_from_config(self.network_arch)
            self.X = self.network_builder.get_X()
            self.Y = tf.placeholder(tf.float32, [None, self.num_classes], name="y")

    @staticmethod
    def from_directory(self, directory):
        pass

    def test(self, input_data):
        with self.graph.as_default() as g:
            return self._test(input_data)

    def _test(self, input_data):

        sess = self.session
        saver = tf.train.Saver()
        if self.model_state_path is not None:
            saver.restore(sess, self.model_state_path)

        input_dict = {}
        for idx in input_data:
            input_dict[self.X[idx]] = input_data[idx]

        r = sess.run([self.model], feed_dict=input_dict)
        r = r[0]
        # normalization, Thanks Stanford C231n
        m = np.max(r, axis=1)
        m.shape = (m.shape[0], 1)
        r -= m
        s = np.sum(np.exp(r), axis=1)
        s.shape = (s.shape[0], 1)
        r = np.exp(r) / s
        r = np.int16(r * 100)
        return r


    def _save(self, saver, sess, roi_gen):

        directory_format = "{0}/convnet_{1}_{2}/"
        filename_format = "model.{0}"

        if len(self.model_meta_information["stats"]["test_accurecy"]) == 0:
            return

        test_X, test_Y, rotations, rois = roi_gen.send(('test', 64, False, True))
        latest_acc = self.model_meta_information["stats"]["test_accurecy"][-1]
        timestr = time.strftime("%Y%m%d-%H%M%S")
        directory_name = directory_format.format('./models/', latest_acc, timestr)
        meta_filename = directory_name + filename_format.format("p")
        ckpt_filename = directory_name + filename_format.format("ckpt")
        test_X_filename = directory_name + "test_data_X.npy"
        test_Y_filename = directory_name + "test_data_Y.npy"
        os.makedirs(directory_name, mode=0o777, exist_ok=True)
        np.save(test_X_filename, test_X)
        np.save(test_Y_filename, test_Y)
        saver.save(sess, ckpt_filename)
        pickle.dump(self.model_meta_information, open(meta_filename, "wb"))

    def train(self, dataset_filename, angles=None, iterations=8e6, batch_size=32,
              drop_out=0.5, reg_power=5e-4, learning_rate=1e-5, engine=None):
        with self.graph.as_default() as g:
            self._train(dataset_filename, angles=angles, iterations=iterations, batch_size=batch_size,
                drop_out=drop_out, reg_power=reg_power, learning_rate=learning_rate, engine=engine
            )

    def _train(self, dataset_filename, angles=None, iterations=8e6, batch_size=32,
              drop_out=0.5, reg_power=5e-4, learning_rate=1e-5, engine=None):

        if angles is None:
            angles = {
                'train': None,
                'validate': None,
                'test': None
            }

        roi_queue = ROIQueue(
            dataset_filename,
            batch_size=batch_size,
            train_angles=angles['train'],
            validate_angles=angles['validate'],
            test_angles=angles['test'],
            engine=engine
        )

        stat_train_acc = []
        stat_test_acc = []
        stat_data_loss = []
        stat_test_loss = []

        sm_stat_train_acc = []
        sm_stat_test_acc = []
        sm_stat_data_loss = []

        full_input_shape = {}
        for subset_id in self.input_shape:
            full_input_shape[subset_id] = [None]
            full_input_shape[subset_id].extend(self.input_shape[subset_id][:])

        sess = self.session
        saver = tf.train.Saver()

        if self.model_state_path is not None:
            saver.restore(sess, self.model_state_path)

        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(self.model, self.Y))
        cost += reg_power * self.network_builder.get_reg()
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

        correct_pred = self.correct_pred = tf.equal(tf.argmax(self.model, 1), tf.argmax(self.Y, 1))
        accuracy = self.accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

        init = tf.initialize_all_variables()
        sess.run(init)

        self.model_meta_information = {
            "type": "convnet",
            "arch": self.network_arch,
            "date": time.time(),
            "input_shape": self.input_shape,
            "num_classes": self.num_classes,
            "iterations_req": iterations,
            "time_start": time.time(),
            "time_end": 0,
            "iterations_done": 0,
            "regularization_strength": reg_power,
            "drop_out_probability": drop_out,
            "batch_size": batch_size,
            "initial_learning_rate": 1e-5,
            "stats": {
                "train_accurecy": None,
                "test_accurecy": None,
                "train_loss": None
            },
            "result": None
        }

        stat_step = 20
        display_step = 20
        train_loss = 0
        step = 1
        logger = tf.train.SummaryWriter('BadLog', sess.graph)

        with roi_queue as roi_generator:
            next(roi_generator)
            try:
                while step * batch_size < iterations:
                    step += 1
                    batch_x, batch_y, roi, rotation = roi_generator.send(('train', batch_size, False, True))
                    train_dict = {
                        self.Y: batch_y
                    }
                    for idx in batch_x:
                        full_input_shape[idx][0] = batch_size
                        batch_x[idx].shape = full_input_shape[idx]
                        train_dict[self.X[idx]] = batch_x[idx]

                    # Run optimization op (backprop)
                    _ = sess.run([optimizer], feed_dict=train_dict)

                    if step % stat_step == 0:
                        test_batch_x, test_batch_y, roi, rotation = roi_generator.send(('test', 32, False, True))
                        test_dict = {
                            self.Y: test_batch_y
                        }
                        for idx in batch_x:
                            full_input_shape[idx][0] = 32
                            test_batch_x[idx].shape = full_input_shape[idx]
                            test_dict[self.X[idx]] = test_batch_x[idx]

                        train_acc, train_loss = sess.run([accuracy, cost], feed_dict=train_dict)
                        test_acc, test_loss = sess.run([accuracy, cost], feed_dict=test_dict)
                        stat_data_loss.append([step * batch_size, train_loss])
                        stat_train_acc.append([step * batch_size, train_acc])
                        stat_test_acc.append([step * batch_size, test_acc])
                        stat_test_loss.append([step * batch_size, test_loss])

                        self.model_meta_information["stats"]["train_accurecy"] = sm_stat_train_acc
                        self.model_meta_information["stats"]["test_accurecy"] = sm_stat_test_acc
                        self.model_meta_information["stats"]["train_loss"] = stat_data_loss
                        self.model_meta_information["result"] = None if len(sm_stat_test_acc) == 0 else sm_stat_test_acc[-1]
                        self.model_meta_information["time_end"] = time.time()
                        self.model_meta_information["iterations_done"] = (step * batch_size)

                    if step % display_step == 0:
                        steps_count = int(display_step / stat_step)
                        tr_acc = int(get_mean(stat_train_acc, steps=steps_count) * 100)
                        ts_acc = int(get_mean(stat_test_acc, steps=steps_count) * 100)
                        sm_stat_train_acc.append([step * batch_size, tr_acc])
                        sm_stat_test_acc.append([step * batch_size, ts_acc])

                        progress = get_mean_diff(stat_train_acc, steps_count)

                        update_graph(stat_data_loss, sm_stat_train_acc, sm_stat_test_acc, stat_test_loss)

                        prg = 0 if progress is None else progress
                        print("Iter " + str(step * batch_size) +
                              ", Loss = {:.6f}".format(train_loss) +
                              ", Training Accuracy = {:.3f}".format(tr_acc) +
                              ", Test Accuracy = {:.3f}".format(ts_acc)
                          )

                self._save(saver, sess, roi_generator)
                print("Optimization Finished!")
            except KeyboardInterrupt:
                test_batch_x, test_batch_y, roi, rotation = roi_generator.send(('test', 92, False, True))
                self.evaluate_model("Hello World!", test_batch_x, test_batch_y)
                self._save(saver, sess, roi_generator)
                logger.close()
                print("Optimization Terminated!")
